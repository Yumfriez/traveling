CREATE TABLE public.countries
(
    id INT PRIMARY KEY NOT NULL,
    name VARCHAR(50) NOT NULL
);
CREATE UNIQUE INDEX countries_name_uindex ON country (name);
CREATE SEQUENCE public.countries_id_seq
  START WITH 1
  INCREMENT BY 1
  NO MINVALUE
  NO MAXVALUE
  CACHE 1;

CREATE TABLE tour_types
(
    id INT PRIMARY KEY NOT NULL,
    name VARCHAR(50) NOT NULL
);
CREATE UNIQUE INDEX tour_types_name_uindex ON tour_type (name);
CREATE SEQUENCE public.tour_types_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE photos
(
    id INT PRIMARY KEY NOT NULL,
    name VARCHAR(50) NOT NULL
);
CREATE SEQUENCE public.photos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE hotels
(
    id INT PRIMARY KEY NOT NULL,
    name VARCHAR(50) NOT NULL,
    phone_number VARCHAR(40) NOT NULL,
    stars INTEGER NOT NULL,
    country_id INT,
    CONSTRAINT hotels_countries_id_fk FOREIGN KEY (country_id) REFERENCES country (id) ON DELETE SET NULL ON UPDATE SET NULL
);
CREATE SEQUENCE public.hotels_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE users
(
    id INT PRIMARY KEY NOT NULL,
    login VARCHAR(50) NOT NULL,
    password VARCHAR(50) NOT NULL
);
CREATE UNIQUE INDEX users_login_uindex ON app_user (login);
CREATE SEQUENCE public.users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE tours
(
    id INT PRIMARY KEY NOT NULL,
    photo_id INT,
    beginning_date DATE NOT NULL,
    duration SMALLINT NOT NULL,
    hotel_id INT NOT NULL,
    tour_type_id INT NOT NULL,
    description VARCHAR(255) NOT NULL,
    price DECIMAL(10,5) NOT NULL,
    CONSTRAINT tours_photos_id_fk FOREIGN KEY (photo_id) REFERENCES photo (id) ON DELETE SET NULL ON UPDATE SET NULL,
    CONSTRAINT tours_hotels_id_fk FOREIGN KEY (hotel_id) REFERENCES hotel (id) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT tours_tour_types_id_fk FOREIGN KEY (tour_type_id) REFERENCES tour_type (id) ON DELETE CASCADE ON UPDATE CASCADE
);
CREATE SEQUENCE public.tours_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE reviews
(
    id INT PRIMARY KEY NOT NULL,
    tour_id INT NOT NULL,
    user_id INT NOT NULL,
    content VARCHAR(255) NOT NULL,
    CONSTRAINT reviews_tours_id_fk FOREIGN KEY (tour_id) REFERENCES tour (id) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT reviews_users_id_fk FOREIGN KEY (user_id) REFERENCES app_user (id) ON DELETE CASCADE ON UPDATE CASCADE
);
CREATE SEQUENCE public.reviews_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE users_tours
(
    user_id INT NOT NULL,
    tour_id INT NOT NULL,
    CONSTRAINT users_tours_users_id_fk FOREIGN KEY (user_id) REFERENCES app_user (id),
    CONSTRAINT users_tours_tours_id_fk FOREIGN KEY (tour_id) REFERENCES tour (id)
);


