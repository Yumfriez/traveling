package com.epam.budaev.travel.service.impl;

import com.epam.budaev.travel.model.Photo;
import com.epam.budaev.travel.repository.PhotoRepository;
import com.epam.budaev.travel.service.PhotoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("photoService")
public class PhotoServiceImpl implements PhotoService {

    private PhotoRepository photoRepository;

    @Autowired
    public void setPhotoRepository(PhotoRepository photoRepository) {
        this.photoRepository = photoRepository;
    }

    @Transactional
    public void addPhoto(Photo photo) {

        photoRepository.save(photo);
    }

    @Transactional
    public void removePhoto(Photo photo) {

        photoRepository.remove(photo);
    }

    @Transactional
    public void updatePhoto(Photo photo) {

        photoRepository.save(photo);
    }

    public Photo findPhotoById(int id) {

        return photoRepository.findById(id);
    }

    public List<Photo> findAllPhotos() {

        return photoRepository.findAll();
    }
}
