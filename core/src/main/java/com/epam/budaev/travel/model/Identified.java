package com.epam.budaev.travel.model;

import java.io.Serializable;

public interface Identified extends Serializable {

    Integer getId();
}
