package com.epam.budaev.travel.service;

import com.epam.budaev.travel.model.Photo;

import java.util.List;

public interface PhotoService {

    void addPhoto(Photo photo);
    void removePhoto(Photo photo);
    void updatePhoto(Photo photo);
    Photo findPhotoById(int id);
    List<Photo> findAllPhotos();
}
