package com.epam.budaev.travel.service;

import com.epam.budaev.travel.model.User;

import java.util.List;

public interface UserService {

    void addUser(User user);
    void removeUser(User user);
    void updateUser(User user);
    User findUserById(int id);
    List<User> findAllUsers();

}
