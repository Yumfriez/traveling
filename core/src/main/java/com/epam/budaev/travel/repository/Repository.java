package com.epam.budaev.travel.repository;

import com.epam.budaev.travel.model.Identified;

import java.util.List;

public interface Repository<T extends Identified> {

    /**
     *
     * @param entity new model for collection
     */
    void save(T entity);

    /**
     *
     * @param entity new model for update
     */
    void update(T entity);

    /**
     *
     * @param entity model for remove
     */
    void remove(T entity);

    /**
     *
     * @param id identification number of model that we need to find
     * @return model, that was found or null if wasn't found
     */
    T findById(int id);

    /**
     *
     * @return get all model from inner collection of repository as List
     */
    List<T> findAll();
}
