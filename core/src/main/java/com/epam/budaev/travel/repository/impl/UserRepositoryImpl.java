package com.epam.budaev.travel.repository.impl;

import com.epam.budaev.travel.model.User;
import com.epam.budaev.travel.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.LockModeType;
import javax.persistence.PersistenceContext;
import java.util.List;


@Repository("userRepository")
@Transactional
public class UserRepositoryImpl implements UserRepository {

    private static final Logger logger = LoggerFactory.getLogger(UserRepositoryImpl.class);
    private static final String USER_LOGGER_LITERAL = "User ";

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void save(User user) {

        entityManager.persist(user);
        logger.info(USER_LOGGER_LITERAL+user.getLogin()+" added in database");
    }

    @Override
    public void update(User user) {
        entityManager.merge(user);
        logger.info(USER_LOGGER_LITERAL+user.getLogin()+" updated in database");
    }

    @Override
    public void remove(User user) {
        entityManager.remove(user);
        logger.info(USER_LOGGER_LITERAL+user.getLogin()+" removed from database");
    }

    @Override
    public User findById(int id) {
        User user = entityManager.find(User.class, id, LockModeType.OPTIMISTIC);
        logger.info(USER_LOGGER_LITERAL+user.getLogin()+" found in database");
        return user;
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<User> findAll() {
        logger.info("Querying all users from database");
        return  entityManager.createNativeQuery("SELECT u.id, u.login, u.password, u.version FROM APP_USER u", User.class)
                .getResultList();
    }

}
