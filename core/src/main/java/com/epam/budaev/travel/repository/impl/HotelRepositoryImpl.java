package com.epam.budaev.travel.repository.impl;

import com.epam.budaev.travel.model.Hotel;
import com.epam.budaev.travel.repository.HotelRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.LockModeType;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository("hotelRepository")
@Transactional
public class HotelRepositoryImpl implements HotelRepository {


    private static final Logger logger = LoggerFactory.getLogger(HotelRepositoryImpl.class);
    private static final String HOTEL_LOGGER_LITERAL = "Hotel ";

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void save(Hotel hotel) {
        entityManager.persist(hotel);
        logger.info(HOTEL_LOGGER_LITERAL+hotel.getName()+" added in database");
    }

    @Override
    public void update(Hotel hotel) {
        entityManager.merge(hotel);
        logger.info(HOTEL_LOGGER_LITERAL+hotel.getName()+" updated in database");
    }

    @Override
    public void remove(Hotel hotel) {

        entityManager.remove(hotel);
        logger.info(HOTEL_LOGGER_LITERAL+hotel.getName()+" removed from database");
    }

    @Override
    public Hotel findById(int id) {

        Hotel hotel = entityManager.find(Hotel.class, id, LockModeType.OPTIMISTIC);
        logger.info(HOTEL_LOGGER_LITERAL+hotel.getName()+" found in database");
        return hotel;
    }

    @Override
    public List<Hotel> findAll() {

        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Hotel> hotelCriteriaQuery = builder.createQuery(Hotel.class);
        Root<Hotel> root = hotelCriteriaQuery.from(Hotel.class);
        hotelCriteriaQuery.select(root);
        logger.info("Querying all hotels from database");
        return entityManager.createQuery(hotelCriteriaQuery).getResultList();
    }
}
