package com.epam.budaev.travel.service.impl;

import com.epam.budaev.travel.model.Hotel;
import com.epam.budaev.travel.repository.HotelRepository;
import com.epam.budaev.travel.service.HotelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("hotelService")
public class HotelServiceImpl implements HotelService {

    private HotelRepository hotelRepository;

    @Autowired
    public void setHotelRepository(HotelRepository hotelRepository) {
        this.hotelRepository = hotelRepository;
    }

    @Transactional
    public void addHotel(Hotel hotel) {

        hotelRepository.save(hotel);
    }

    @Transactional
    public void removeHotel(Hotel hotel) {

        hotelRepository.remove(hotel);
    }

    @Transactional
    public void updateHotel(Hotel hotel) {

        hotelRepository.save(hotel);
    }

    public Hotel findHotelById(int id) {

        return hotelRepository.findById(id);
    }

    public List<Hotel> findAllHotels() {

        return hotelRepository.findAll();
    }
}
