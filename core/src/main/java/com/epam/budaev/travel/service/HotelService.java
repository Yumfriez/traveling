package com.epam.budaev.travel.service;

import com.epam.budaev.travel.model.Hotel;

import java.util.List;

public interface HotelService {

    void addHotel(Hotel hotel);
    void removeHotel(Hotel hotel);
    void updateHotel(Hotel hotel);
    Hotel findHotelById(int id);
    List<Hotel> findAllHotels();
}
