package com.epam.budaev.travel.service.impl;

import com.epam.budaev.travel.dto.TourDto;
import com.epam.budaev.travel.exception.TravelAgencyException;
import com.epam.budaev.travel.model.Tour;
import com.epam.budaev.travel.repository.TourRepository;
import com.epam.budaev.travel.service.TourService;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.NotNull;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

@Service("tourService")
public class TourServiceImpl implements TourService {

    private final static Logger logger = LoggerFactory.getLogger(TourServiceImpl.class);

    private TourRepository tourRepository;

    @Autowired
    public void setTourRepository(TourRepository tourRepository) {
        this.tourRepository = tourRepository;
    }

    @Transactional
    public void addTour(Tour tour) {

        tourRepository.save(tour);
    }

    @Transactional
    public void removeTour(Tour tour) {

        tourRepository.remove(tour);
    }

    @Transactional
    public void updateTour(Tour tour) {

        tourRepository.save(tour);
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @Override
    public void loadToursFromFile(String filePath) {
        ObjectMapper objectMapper = new ObjectMapper().registerModule(new JavaTimeModule());
        TypeReference<List<Tour>> typeReference = new TypeReference<List<Tour>>(){};
        try(InputStream inputStream = new FileInputStream(filePath)) {
            List<Tour> tours = objectMapper.readValue(inputStream, typeReference);
            for(Tour tour: tours) {
                saveOrUpdate(tour);
            }
        } catch (FileNotFoundException e) {
            logger.error(e.getMessage());
            throw new TravelAgencyException("Could not find file with tours to load", e);
        } catch (IOException e) {
            logger.error(e.getMessage());
            throw new TravelAgencyException("Error during file with tours reading", e);
        }
    }

    public Tour findTourById(int id) {

        return tourRepository.findById(id);
    }

    @Override
    public List<Tour> findToursByCriteria(TourDto tourDto) {
        return tourRepository.findToursByCriteria(tourDto);
    }

    public List<Tour> findAllTours() {

        return tourRepository.findAll();
    }


    private void saveOrUpdate(@NotNull Tour tour) {
        if(tour.getId() == null) {
            tourRepository.save(tour);
        } else {
            tourRepository.update(tour);
        }
    }
}
