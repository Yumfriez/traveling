package com.epam.budaev.travel.repository;

import com.epam.budaev.travel.model.User;

public interface UserRepository extends Repository<User>{
}
