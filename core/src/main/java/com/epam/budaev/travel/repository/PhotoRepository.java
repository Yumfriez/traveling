package com.epam.budaev.travel.repository;

import com.epam.budaev.travel.model.Photo;

public interface PhotoRepository extends Repository<Photo>{
}
