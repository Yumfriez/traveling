package com.epam.budaev.travel.repository.impl;

import com.epam.budaev.travel.model.Country;
import com.epam.budaev.travel.repository.CountryRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.LockModeType;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;


@Repository("countryRepository")
@Transactional
public class CountryRepositoryImpl implements CountryRepository {
    private static final Logger logger = LoggerFactory.getLogger(CountryRepositoryImpl.class);
    private static final String COUNTRY_LOGGER_LITERAL = "Country ";

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void save(Country country) {
        entityManager.persist(country);
        logger.info(COUNTRY_LOGGER_LITERAL+country.getName()+" added in database");
    }

    @Override
    public void update(Country country) {
        entityManager.merge(country);
        logger.info(COUNTRY_LOGGER_LITERAL+country.getName()+" updated in database");
    }

    @Override
    public void remove(Country country) {
        entityManager.remove(country);
        logger.info(COUNTRY_LOGGER_LITERAL+country.getName()+" removed from database");
    }

    @Override
    public Country findById(int id) {
        Country country = entityManager.find(Country.class, id, LockModeType.OPTIMISTIC);
        logger.info(COUNTRY_LOGGER_LITERAL+country.getName()+" found in database");
        return country;
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Country> findAll() {
        logger.info("Querying all countries from database");
        return entityManager.createNamedQuery("Country.getAllCountries")
                .getResultList();
    }

    @Override
    public List<Country> findCountriesByName(String name) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Country> countryCriteriaQuery = builder.createQuery(Country.class);
        Root<Country> root = countryCriteriaQuery.from(Country.class);
        countryCriteriaQuery.select(root)
                .where(builder.equal(root.get("name"), name));
        logger.info("Querying all countries with name: "+name+" from database");
        return entityManager.createQuery(countryCriteriaQuery).getResultList();
    }
}
