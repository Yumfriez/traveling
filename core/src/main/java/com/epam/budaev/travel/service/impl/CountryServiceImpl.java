package com.epam.budaev.travel.service.impl;

import com.epam.budaev.travel.model.Country;
import com.epam.budaev.travel.repository.CountryRepository;
import com.epam.budaev.travel.service.CountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("countryService")
public class CountryServiceImpl implements CountryService {

    private final CountryRepository countryRepository;

    @Autowired
    public CountryServiceImpl(CountryRepository countryRepository) {
        this.countryRepository = countryRepository;
    }

    @Transactional
    public void addCountry(Country country) {
        countryRepository.save(country);
    }

    @Transactional
    public void removeCountry(Country country) {

        countryRepository.remove(country);
    }

    @Transactional
    public void updateCountry(Country country) {

        countryRepository.save(country);
    }

    public Country findCountryById(int id) {

        return countryRepository.findById(id);
    }


    public List<Country> findAllCountries() {

        return countryRepository.findAll();
    }
}
