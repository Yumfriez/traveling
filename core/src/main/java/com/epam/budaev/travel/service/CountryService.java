package com.epam.budaev.travel.service;

import com.epam.budaev.travel.model.Country;

import java.util.List;

public interface CountryService {

    void addCountry(Country country);
    void removeCountry(Country country);
    void updateCountry(Country country);
    Country findCountryById(int id);
    List<Country> findAllCountries();
}
