package com.epam.budaev.travel.service.impl;

import com.epam.budaev.travel.model.TourType;
import com.epam.budaev.travel.repository.TourTypeRepository;
import com.epam.budaev.travel.service.TourTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("tourTypeService")
public class TourTypeServiceImpl implements TourTypeService {

    private TourTypeRepository tourTypeRepository;

    @Autowired
    public void setTourTypeRepository(TourTypeRepository tourTypeRepository) {
        this.tourTypeRepository = tourTypeRepository;
    }

    @Transactional
    public void addTourType(TourType tourType) {

        tourTypeRepository.save(tourType);
    }

    @Transactional
    public void removeTourType(TourType tourType) {

        tourTypeRepository.remove(tourType);
    }

    @Transactional
    public void updateTourType(TourType tourType) {

        tourTypeRepository.save(tourType);
    }

    public TourType findTourTypeById(int id) {

        return tourTypeRepository.findById(id);
    }

    public List<TourType> findAllTourTypes() {

        return tourTypeRepository.findAll();
    }
}
