package com.epam.budaev.travel.service.impl;

import com.epam.budaev.travel.model.User;
import com.epam.budaev.travel.repository.UserRepository;
import com.epam.budaev.travel.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("userService")
public class UserServiceImpl implements UserService {

    private UserRepository userRepository;

    @Autowired
    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Transactional
    public void addUser(User user) {

        userRepository.save(user);
    }

    @Transactional
    public void removeUser(User user) {

        userRepository.remove(user);
    }

    @Transactional
    public void updateUser(User user) {

        userRepository.save(user);
    }

    public User findUserById(int id) {

        return userRepository.findById(id);
    }

    public List<User> findAllUsers() {

        return userRepository.findAll();
    }
}
