package com.epam.budaev.travel.repository;

import com.epam.budaev.travel.model.Country;

import java.util.List;

public interface CountryRepository extends Repository<Country> {

    List<Country> findCountriesByName(String name);
}
