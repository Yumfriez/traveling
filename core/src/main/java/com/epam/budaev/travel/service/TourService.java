package com.epam.budaev.travel.service;

import com.epam.budaev.travel.dto.TourDto;
import com.epam.budaev.travel.model.Tour;

import java.util.List;

public interface TourService {

    void addTour(Tour tour);
    void removeTour(Tour tour);
    void updateTour(Tour tour);
    void loadToursFromFile(String filePath);
    Tour findTourById(int id);
    List<Tour> findToursByCriteria(TourDto tourDto);
    List<Tour> findAllTours();
}
