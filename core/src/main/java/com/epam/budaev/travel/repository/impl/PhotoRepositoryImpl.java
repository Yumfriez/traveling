package com.epam.budaev.travel.repository.impl;

import com.epam.budaev.travel.model.Photo;
import com.epam.budaev.travel.repository.PhotoRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.LockModeType;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository("photoRepository")
@Transactional
public class PhotoRepositoryImpl implements PhotoRepository {

    private static final Logger logger = LoggerFactory.getLogger(PhotoRepositoryImpl.class);
    private static final String PHOTO_LOGGER_LITERAL = "Photo ";

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void save(Photo photo) {
        entityManager.persist(photo);
        logger.info(PHOTO_LOGGER_LITERAL+photo.getName()+" added in database");

    }

    @Override
    public void update(Photo photo) {
        entityManager.merge(photo);
        logger.info(PHOTO_LOGGER_LITERAL+photo.getName()+" updated in database");
    }

    @Override
    public void remove(Photo photo) {
        entityManager.remove(photo);
        logger.info(PHOTO_LOGGER_LITERAL+photo.getName()+" removed from database");
    }

    @Override
    public Photo findById(int id) {

        Photo photo = entityManager.find(Photo.class, id, LockModeType.OPTIMISTIC);
        logger.info(PHOTO_LOGGER_LITERAL+photo.getName()+" found in database");
        return photo;
    }

    @Override
    public List<Photo> findAll() {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Photo> photoCriteriaQuery = builder.createQuery(Photo.class);
        Root<Photo> root = photoCriteriaQuery.from(Photo.class);
        photoCriteriaQuery.select(root);
        logger.info("Querying all photos from database");
        return entityManager.createQuery(photoCriteriaQuery).getResultList();
    }
}
