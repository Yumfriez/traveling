package com.epam.budaev.travel.repository.impl;

import com.epam.budaev.travel.model.Review;
import com.epam.budaev.travel.repository.ReviewRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.LockModeType;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;


@Repository("reviewRepository")
@Transactional
public class ReviewRepositoryImpl implements ReviewRepository {

    private static final Logger logger = LoggerFactory.getLogger(ReviewRepositoryImpl.class);
    private static final String REVIEW_LOGGER_LITERAL = "Review ";

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void save(Review review) {
        entityManager.persist(review);
        logger.info(REVIEW_LOGGER_LITERAL+review.getId()+" added in database");

    }

    @Override
    public void update(Review review) {
        entityManager.merge(review);
        logger.info(REVIEW_LOGGER_LITERAL+review.getId()+" updated in database");
    }

    @Override
    public void remove(Review review) {
        entityManager.remove(review);
        logger.info(REVIEW_LOGGER_LITERAL+review.getId()+" removed from database");
    }

    @Override
    public Review findById(int id) {

        Review review = entityManager.find(Review.class, id, LockModeType.OPTIMISTIC);
        logger.info(REVIEW_LOGGER_LITERAL+review.getId()+" found in database");
        return review;
    }

    @Override
    public List<Review> findAll() {

        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Review> reviewCriteriaQuery = builder.createQuery(Review.class);
        Root<Review> root = reviewCriteriaQuery.from(Review.class);
        reviewCriteriaQuery.select(root);
        logger.info("Querying all reviews from database");
        return entityManager.createQuery(reviewCriteriaQuery).getResultList();
    }
}
