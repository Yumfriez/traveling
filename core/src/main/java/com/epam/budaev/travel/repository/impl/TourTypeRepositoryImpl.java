package com.epam.budaev.travel.repository.impl;

import com.epam.budaev.travel.model.TourType;
import com.epam.budaev.travel.repository.TourTypeRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.LockModeType;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;


@Repository("tourTypeRepository")
@Transactional
public class TourTypeRepositoryImpl implements TourTypeRepository {


    private static final Logger logger = LoggerFactory.getLogger(TourTypeRepositoryImpl.class);
    private static final String TOUR_TYPE_LOGGER_LITERAL = "TourType ";

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void save(TourType tourType) {
        entityManager.persist(tourType);
        logger.info(TOUR_TYPE_LOGGER_LITERAL+tourType.getName()+" added in database");
    }

    @Override
    public void update(TourType tourType) {
        entityManager.merge(tourType);
        logger.info(TOUR_TYPE_LOGGER_LITERAL+tourType.getName()+" updated in database");
    }

    @Override
    public void remove(TourType tourType) {
        entityManager.remove(tourType);
        logger.info(TOUR_TYPE_LOGGER_LITERAL+tourType.getName()+" removed from database");
    }

    @Override
    public TourType findById(int id) {

        TourType tourType = entityManager.find(TourType.class, id, LockModeType.OPTIMISTIC);
        logger.info(TOUR_TYPE_LOGGER_LITERAL+tourType.getName()+" found in database");
        return tourType;
    }

    @Override
    public List<TourType> findAll() {

        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<TourType> tourTypeCriteriaQuery = builder.createQuery(TourType.class);
        Root<TourType> root = tourTypeCriteriaQuery.from(TourType.class);
        tourTypeCriteriaQuery.select(root);
        logger.info("Querying all tourTypes from database");
        return entityManager.createQuery(tourTypeCriteriaQuery).getResultList();
    }
}
