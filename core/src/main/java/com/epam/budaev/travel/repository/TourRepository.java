package com.epam.budaev.travel.repository;

import com.epam.budaev.travel.dto.TourDto;
import com.epam.budaev.travel.model.Tour;

import java.util.List;

public interface TourRepository extends Repository<Tour>{

    List<Tour> findToursByCriteria(TourDto tourDto);
}
