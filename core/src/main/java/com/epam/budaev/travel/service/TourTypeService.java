package com.epam.budaev.travel.service;

import com.epam.budaev.travel.model.TourType;

import java.util.List;

public interface TourTypeService {

    void addTourType(TourType tourType);
    void removeTourType(TourType tourType);
    void updateTourType(TourType tourType);
    TourType findTourTypeById(int id);
    List<TourType> findAllTourTypes();
}
