package com.epam.budaev.travel.model;


import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.sql.Date;
import java.time.LocalDate;
import java.util.Objects;

@Entity
@Table(name = "TOUR")
@Cacheable
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Tour implements Identified{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "PHOTO_ID")
    private Photo photo;

    @Column(name = "BEGINNING_DATE")
    private Date beginningDate;

    private Integer duration;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "HOTEL_ID")
    private Hotel hotel;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "TOUR_TYPE_ID")
    private TourType tourType;

    @NotEmpty
    @Size(min = 5, max = 600, message = "Wrong tour description length")
    private String description;

    @Column(name = "PRICE")
    private BigDecimal cost;

    @Version
    private int version;

    public Tour() {
        /*
          default constructor
         */
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Photo getPhoto() {
        return photo;
    }

    public void setPhoto(Photo photo) {
        this.photo = photo;
    }

    public Date getBeginningDate() {
        return beginningDate;
    }

    public void setBeginningDate(LocalDate beginningDate) {
        this.beginningDate = Date.valueOf(beginningDate);
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public Hotel getHotel() {
        return hotel;
    }

    public void setHotel(Hotel hotel) {
        this.hotel = hotel;
    }

    public TourType getTourType() {
        return tourType;
    }

    public void setTourType(TourType tourType) {
        this.tourType = tourType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getCost() {
        return cost;
    }

    public void setCost(BigDecimal cost) {
        this.cost = cost;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Tour tour = (Tour) o;
        return version == tour.version &&
                Objects.equals(id, tour.id) &&
                Objects.equals(photo, tour.photo) &&
                Objects.equals(beginningDate, tour.beginningDate) &&
                Objects.equals(duration, tour.duration) &&
                Objects.equals(hotel, tour.hotel) &&
                Objects.equals(tourType, tour.tourType) &&
                Objects.equals(description, tour.description) &&
                Objects.equals(cost, tour.cost);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, photo, beginningDate, duration, hotel, tourType, description, cost);
    }

    @Override
    public String toString() {
        return "Tour{" +
                "id=" + id +
                ", photo=" + photo +
                ", beginningDate=" + beginningDate +
                ", duration=" + duration +
                ", hotel=" + hotel +
                ", tourType=" + tourType +
                ", description='" + description + '\'' +
                ", cost=" + cost +
                ", version=" + version +
                '}';
    }
}
