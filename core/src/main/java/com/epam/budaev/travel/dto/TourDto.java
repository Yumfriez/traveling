package com.epam.budaev.travel.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;
import java.util.Objects;

public class TourDto implements Serializable {

    private Date beginningDate;

    private Integer duration;

    private Integer stars;

    private String tourTypeName;

    private BigDecimal cost;

    private String countryName;

    public TourDto() {
        /*
          default constructor
         */
    }

    public Date getBeginningDate() {
        return beginningDate;
    }

    public void setBeginningDate(Date beginningDate) {
        this.beginningDate = beginningDate;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public Integer getStars() {
        return stars;
    }

    public void setStars(Integer stars) {
        this.stars = stars;
    }

    public String getTourTypeName() {
        return tourTypeName;
    }

    public void setTourTypeName(String tourTypeName) {
        this.tourTypeName = tourTypeName;
    }

    public BigDecimal getCost() {
        return cost;
    }

    public void setCost(BigDecimal cost) {
        this.cost = cost;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TourDto tourDto = (TourDto) o;
        return Objects.equals(beginningDate, tourDto.beginningDate) &&
                Objects.equals(duration, tourDto.duration) &&
                Objects.equals(stars, tourDto.stars) &&
                Objects.equals(tourTypeName, tourDto.tourTypeName) &&
                Objects.equals(cost, tourDto.cost);
    }

    @Override
    public int hashCode() {
        return Objects.hash(beginningDate, duration, stars, tourTypeName, cost);
    }

    @Override
    public String toString() {
        return "TourDto{" +
                "beginningDate=" + beginningDate +
                ", duration=" + duration +
                ", stars=" + stars +
                ", tourTypeName='" + tourTypeName + '\'' +
                ", cost=" + cost +
                '}';
    }
}
