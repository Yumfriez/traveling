package com.epam.budaev.travel.repository;

import com.epam.budaev.travel.model.Hotel;

public interface HotelRepository extends Repository<Hotel> {
}
