package com.epam.budaev.travel.repository;

import com.epam.budaev.travel.model.TourType;

public interface TourTypeRepository extends Repository<TourType>{
}
