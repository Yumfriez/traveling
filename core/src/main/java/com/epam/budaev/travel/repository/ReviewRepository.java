package com.epam.budaev.travel.repository;

import com.epam.budaev.travel.model.Review;

public interface ReviewRepository extends Repository<Review>{
}
