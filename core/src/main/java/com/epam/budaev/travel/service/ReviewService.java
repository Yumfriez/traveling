package com.epam.budaev.travel.service;

import com.epam.budaev.travel.model.Review;

import java.util.List;

public interface ReviewService {

    void addReview(Review review);
    void removeReview(Review review);
    void updateReview(Review review);
    Review findReviewById(int id);
    List<Review> findAllReviews();
}
