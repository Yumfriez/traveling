package com.epam.budaev.travel.repository.impl;

import com.epam.budaev.travel.dto.TourDto;
import com.epam.budaev.travel.model.Country;
import com.epam.budaev.travel.model.Hotel;
import com.epam.budaev.travel.model.Tour;
import com.epam.budaev.travel.model.TourType;
import com.epam.budaev.travel.repository.TourRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.LockModeType;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.*;
import java.util.List;


@Repository("tourRepository")
@Transactional
public class TourRepositoryImpl implements TourRepository {

    private static final Logger logger = LoggerFactory.getLogger(TourRepositoryImpl.class);
    private static final String TOUR_LOGGER_LITERAL = "Tour ";

    @PersistenceContext
    private EntityManager entityManager;


    @Override
    public void save(Tour tour) {
        entityManager.persist(tour);
        logger.info(TOUR_LOGGER_LITERAL+tour.getId()+" added in database");

    }

    @Override
    public void update(Tour tour) {
        entityManager.merge(tour);
        logger.info(TOUR_LOGGER_LITERAL+tour.getId()+" updated in database");
    }

    @Override
    public void remove(Tour tour) {
        entityManager.remove(tour);
        logger.info(TOUR_LOGGER_LITERAL+tour.getId()+" removed from database");
    }

    @Override
    public Tour findById(int id) {
        Tour tour = entityManager.find(Tour.class, id, LockModeType.OPTIMISTIC);
        logger.info(TOUR_LOGGER_LITERAL+tour.getId()+" found in database");
        return tour;
    }

    @Override
    public List<Tour> findAll() {

        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Tour> tourCriteriaQuery = builder.createQuery(Tour.class);
        Root<Tour> root = tourCriteriaQuery.from(Tour.class);
        tourCriteriaQuery.select(root);
        logger.info("Querying all tours from database");
        return entityManager.createQuery(tourCriteriaQuery).getResultList();
    }

    @Override
    public List<Tour> findToursByCriteria(TourDto tourDto) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Tour> tourCriteriaQuery = builder.createQuery(Tour.class);
        Root<Tour> root = tourCriteriaQuery.from(Tour.class);

        Join<Tour, TourType> tourTourTypeJoin = root.join("tourType");
        Join<Tour, Hotel> tourHotelJoin = root.join("hotel");
        Join<Tour, Country> tourCountryJoin = root.join("hotel").join("country");

        tourCriteriaQuery.where(builder.equal(tourCountryJoin.get("name"), tourDto.getCountryName()),
                builder.equal(tourHotelJoin.get("stars"), tourDto.getStars()),
                builder.equal(tourTourTypeJoin.get("name"), tourDto.getTourTypeName()),
                builder.equal(root.get("beginningDate"), tourDto.getBeginningDate()),
                builder.equal(root.get("duration"), tourDto.getDuration()));
        logger.info("Querying tours from database");
        return entityManager.createQuery(tourCriteriaQuery).getResultList();
    }
}
