package com.epam.budaev.travel.exception;

public class TravelAgencyException extends RuntimeException {

    public TravelAgencyException(String message) {
        super(message);
    }

    public TravelAgencyException(String message, Throwable cause) {
        super(message, cause);
    }
}
