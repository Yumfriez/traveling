package com.epam.budaev.travel.service.impl;

import com.epam.budaev.travel.model.Review;
import com.epam.budaev.travel.repository.ReviewRepository;
import com.epam.budaev.travel.service.ReviewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("reviewService")
public class ReviewServiceImpl implements ReviewService {

    private ReviewRepository reviewRepository;

    @Autowired
    public void setReviewRepository(ReviewRepository reviewRepository) {
        this.reviewRepository = reviewRepository;
    }

    @Transactional
    public void addReview(Review review) {

        reviewRepository.save(review);
    }

    @Transactional
    public void removeReview(Review review) {

        reviewRepository.remove(review);
    }

    @Transactional
    public void updateReview(Review review) {

        reviewRepository.save(review);
    }

    public Review findReviewById(int id) {

        return reviewRepository.findById(id);
    }

    public List<Review> findAllReviews() {

        return reviewRepository.findAll();
    }
}
