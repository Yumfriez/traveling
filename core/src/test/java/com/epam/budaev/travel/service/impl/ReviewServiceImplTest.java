package com.epam.budaev.travel.service.impl;

import com.epam.budaev.travel.model.Review;
import com.epam.budaev.travel.repository.ReviewRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Answers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

public class ReviewServiceImplTest {

    @Mock(answer = Answers.RETURNS_SMART_NULLS)
    private ReviewRepository reviewRepository;

    @InjectMocks
    private ReviewServiceImpl reviewService;

    @Before
    public void setUp()  {

        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void addReview()  {
        Review review = new Review();
        review.setContent("Really cool!");
        reviewService.addReview(review);

        verify(reviewRepository).save(review);
    }

    @Test
    public void removeReview()  {
        Review review = new Review();

        reviewService.removeReview(review);

        verify(reviewRepository).remove(review);
    }

    @Test
    public void updateReview()  {

        Review review = new Review();
        review.setContent("Too boring!");

        reviewService.updateReview(review);

        verify(reviewRepository).save(review);
    }

    @Test
    public void findReviewById()  {
        final int id = 222;

        doReturn(new Review()).when(reviewRepository).findById(id);

        Review review = reviewService.findReviewById(id);

        verify(reviewRepository).findById(222);
        assertThat(review, is(notNullValue()));
    }

    @Test
    public void findAllReviews()  {

        doReturn(new ArrayList<Review>()).when(reviewRepository).findAll();

        List<Review> reviews = reviewService.findAllReviews();

        verify(reviewRepository).findAll();
        assertThat(reviews, is(notNullValue()));
    }

}