package com.epam.budaev.travel.repository.impl;

import com.epam.budaev.travel.config.TestConfiguration;
import com.epam.budaev.travel.model.User;
import com.epam.budaev.travel.repository.UserRepository;
import com.epam.budaev.travel.repository.impl.util.SqlRunner;
import org.hsqldb.cmdline.SqlToolError;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestConfiguration.class })
public class UserRepositoryImplTest {

    @Mock
    private UserRepository mockedUserRepository;

    @Autowired
    private UserRepository userRepository;


    @Before
    public void init() throws SQLException, ClassNotFoundException, IOException, SqlToolError {
        initMocks(this);
        Class.forName("org.hsqldb.jdbc.JDBCDriver");
        runSqlScript("/test-create-script.sql");
    }


    @After
    public void destroy() throws SQLException, IOException, SqlToolError {
        runSqlScript("/test-dropall-script.sql");
    }

    private void runSqlScript(String scriptPath) throws SQLException, IOException, SqlToolError {
        try (Connection connection = getConnection()) {
            new SqlRunner().runSqlScript(connection, scriptPath);
        }
    }

    private static Connection getConnection() throws SQLException {
        return DriverManager.getConnection("jdbc:hsqldb:mem:traveling-agency", "test", "test");
    }


    @Test
    public void shouldReturnUserWhenGetById(){
        User user = userRepository.findById(1);
        assertThat(user.getLogin(), is("Malachite"));
    }

    @Test
    public void shouldGenerateIdWhenSaveUser(){
        User user = new User();
        user.setLogin("Hellboy");
        user.setPassword("asd123123");
        userRepository.save(user);
        assertThat(user.getId(), is(notNullValue()));
        assertThat(user.getId(), is(4));
    }

    @Test
    public void shouldEditUserWhenUpdate(){
        User user = userRepository.findById(1);
        User userForUpdate = new User();
        userForUpdate.setId(user.getId());
        userForUpdate.setLogin(user.getLogin());
        userForUpdate.setPassword("cOoOL123");
        userForUpdate.setVersion(user.getVersion());
        userRepository.update(userForUpdate);

        User updatedUser = userRepository.findById(1);

        assertThat(updatedUser.getPassword(), is("cOoOL123"));
    }

    @Test
    public void shouldRemoveUserWhenFoundById(){
        User user = new User();
        user.setId(1);

        mockedUserRepository.remove(user);

        verify(mockedUserRepository).remove(user);

    }

    @Test
    public void shouldQueryAllUsers(){
        List<User> userList = userRepository.findAll();

        assertThat(userList.size(), is(3));
    }




}