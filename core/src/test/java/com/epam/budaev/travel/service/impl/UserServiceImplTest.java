package com.epam.budaev.travel.service.impl;

import com.epam.budaev.travel.model.User;
import com.epam.budaev.travel.repository.UserRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Answers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

public class UserServiceImplTest {

    @Mock(answer = Answers.RETURNS_SMART_NULLS)
    private UserRepository userRepository;

    @InjectMocks
    private UserServiceImpl userService;

    @Before
    public void setUp()  {

        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void addUser()  {

        User user = new User();
        user.setPassword("asdasd");

        userService.addUser(user);

        verify(userRepository).save(user);

    }

    @Test
    public void removeUser()  {
        User user = new User();

        userService.removeUser(user);

        verify(userRepository).remove(user);
    }

    @Test
    public void updateUser()  {

        User user = new User();
        user.setPassword("asd123");

        userService.updateUser(user);

        verify(userRepository).save(user);
    }

    @Test
    public void findUserById()  {

        final int id = 222;

        doReturn(new User()).when(userRepository).findById(id);

        User user = userService.findUserById(id);

        verify(userRepository).findById(222);
        assertThat(user, is(notNullValue()));
    }

    @Test
    public void findAllUsers()  {
        doReturn(new ArrayList<User>()).when(userRepository).findAll();

        List<User> users = userService.findAllUsers();

        verify(userRepository).findAll();
        assertThat(users, is(notNullValue()));
    }

}