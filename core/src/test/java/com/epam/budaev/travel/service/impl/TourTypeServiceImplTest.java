package com.epam.budaev.travel.service.impl;

import com.epam.budaev.travel.model.TourType;
import com.epam.budaev.travel.repository.TourTypeRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Answers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

public class TourTypeServiceImplTest {

    @Mock(answer = Answers.RETURNS_SMART_NULLS)
    private TourTypeRepository tourTypeRepository;

    @InjectMocks
    private TourTypeServiceImpl tourTypeService;

    @Before
    public void setUp()  {

        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void addTourType()  {
        TourType tourType = new TourType();
        tourType.setName("Yahoo");

        tourTypeService.addTourType(tourType);

        verify(tourTypeRepository).save(tourType);
    }

    @Test
    public void removeTourType()  {
        TourType tourType = new TourType();

        tourTypeService.removeTourType(tourType);

        verify(tourTypeRepository).remove(tourType);
    }

    @Test
    public void updateTourType()  {

        TourType tourType = new TourType();
        tourType.setName("Uganda");

        tourTypeService.updateTourType(tourType);

        verify(tourTypeRepository).save(tourType);
    }

    @Test
    public void findTourTypeById()  {

        final int id = 222;

        doReturn(new TourType()).when(tourTypeRepository).findById(id);

        TourType tourType = tourTypeService.findTourTypeById(id);

        verify(tourTypeRepository).findById(222);
        assertThat(tourType, is(notNullValue()));
    }

    @Test
    public void findAllTourTypes()  {
        doReturn(new ArrayList<TourType>()).when(tourTypeRepository).findAll();

        List<TourType> tourTypes = tourTypeService.findAllTourTypes();

        verify(tourTypeRepository).findAll();
        assertThat(tourTypes, is(notNullValue()));
    }

}