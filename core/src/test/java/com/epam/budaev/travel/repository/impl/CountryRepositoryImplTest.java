package com.epam.budaev.travel.repository.impl;

import com.epam.budaev.travel.config.TestConfiguration;
import com.epam.budaev.travel.model.Country;
import com.epam.budaev.travel.repository.CountryRepository;
import com.epam.budaev.travel.repository.impl.util.SqlRunner;
import org.hsqldb.cmdline.SqlToolError;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestConfiguration.class })
public class CountryRepositoryImplTest {

    @Mock
    private CountryRepository mockedCountryRepository;

    @Autowired
    private CountryRepository countryRepository;


    @Before
    public void init() throws SQLException, ClassNotFoundException, IOException, SqlToolError {
        initMocks(this);
        Class.forName("org.hsqldb.jdbc.JDBCDriver");
        runSqlScript("/test-create-script.sql");
    }


    @After
    public void destroy() throws SQLException, IOException, SqlToolError {
        runSqlScript("/test-dropall-script.sql");
    }

    private void runSqlScript(String scriptPath) throws SQLException, IOException, SqlToolError {
        try (Connection connection = getConnection()) {
            new SqlRunner().runSqlScript(connection, scriptPath);
        }
    }

    private static Connection getConnection() throws SQLException {
        return DriverManager.getConnection("jdbc:hsqldb:mem:traveling-agency", "test", "test");
    }


    @Test
    public void shouldReturnCountryWhenGetById(){
        Country country = countryRepository.findById(1);
        assertThat(country.getName(), is("Belarus"));
    }

    @Test
    public void shouldGenerateIdWhenSaveCountry(){
        Country country = new Country();
        country.setName("Norway");
        countryRepository.save(country);
        assertThat(country.getId(), is(notNullValue()));
        assertThat(country.getId(), is(4));
    }

    @Test
    public void shouldEditCountryWhenUpdate(){
        Country country = countryRepository.findById(1);
        Country countryForUpdate = new Country();
        countryForUpdate.setId(country.getId());
        countryForUpdate.setName("Poland");
        countryForUpdate.setVersion(country.getVersion());
        countryRepository.update(countryForUpdate);

        Country updatedCountry = countryRepository.findById(1);

        assertThat(updatedCountry.getName(), is("Poland"));
    }

    @Test
    public void shouldRemoveCountryWhenFoundById(){
        Country country = new Country();
        country.setId(1);

        mockedCountryRepository.remove(country);

        verify(mockedCountryRepository).remove(country);

    }

    @Test
    public void shouldQueryAllCountries(){
        List<Country> countryList = countryRepository.findAll();

        assertThat(countryList.size(), is(3));
    }




}
