package com.epam.budaev.travel.service.impl;

import com.epam.budaev.travel.model.Hotel;
import com.epam.budaev.travel.repository.HotelRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Answers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

public class HotelServiceImplTest {

    @Mock(answer = Answers.RETURNS_SMART_NULLS)
    private HotelRepository hotelRepository;

    @InjectMocks
    private HotelServiceImpl hotelService;


    @Before
    public void setUp() {

        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void addHotel() {
        Hotel hotel = new Hotel();
        hotel.setName("Venus");

        hotelService.addHotel(hotel);

        verify(hotelRepository).save(hotel);
    }

    @Test
    public void removeHotel() {
        Hotel hotel = new Hotel();
        hotel.setId(1);

        hotelService.removeHotel(hotel);
        verify(hotelRepository).remove(hotel);
    }

    @Test
    public void updateHotel() {

        Hotel hotel = new Hotel();
        hotel.setName("Pacific");

        hotelService.updateHotel(hotel);

        verify(hotelRepository).save(hotel);
    }

    @Test
    public void findHotelById() {

        final int id = 222;

        doReturn(new Hotel()).when(hotelRepository).findById(id);

        Hotel hotel = hotelService.findHotelById(id);

        verify(hotelRepository).findById(222);
        assertThat(hotel, is(notNullValue()));
    }

    @Test
    public void findAllHotels() {

        doReturn(new ArrayList<Hotel>()).when(hotelRepository).findAll();

        List<Hotel> hotels = hotelService.findAllHotels();

        verify(hotelRepository).findAll();
        assertThat(hotels, is(notNullValue()));
    }



}