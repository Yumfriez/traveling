package com.epam.budaev.travel.service.impl;

import com.epam.budaev.travel.model.Tour;
import com.epam.budaev.travel.repository.TourRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Answers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

public class TourServiceImplTest {

    @Mock(answer = Answers.RETURNS_SMART_NULLS)
    private TourRepository tourRepository;

    @InjectMocks
    private TourServiceImpl tourService;

    @Before
    public void setUp()  {

        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void addTour()  {

        Tour tour = new Tour();
        tour.setDescription("Tour to the Europe!");

        tourService.addTour(tour);

        verify(tourRepository).save(tour);
    }

    @Test
    public void removeTour()  {
        Tour tour = new Tour();

        tourService.removeTour(tour);

        verify(tourRepository).remove(tour);
    }

    @Test
    public void updateTour()  {
        Tour tour = new Tour();
        tour.setDescription("Tour to the USA!");

        tourService.updateTour(tour);

        verify(tourRepository).save(tour);
    }

    @Test
    public void findTourById()  {

        final int id = 222;

        doReturn(new Tour()).when(tourRepository).findById(id);

        Tour tour = tourService.findTourById(id);

        verify(tourRepository).findById(222);
        assertThat(tour, is(notNullValue()));
    }

    @Test
    public void findAllTours()  {

        doReturn(new ArrayList<Tour>()).when(tourRepository).findAll();

        List<Tour> tours = tourService.findAllTours();

        verify(tourRepository).findAll();
        assertThat(tours, is(notNullValue()));
    }

}