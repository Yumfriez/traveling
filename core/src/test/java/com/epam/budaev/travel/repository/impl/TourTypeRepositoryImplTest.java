package com.epam.budaev.travel.repository.impl;

import com.epam.budaev.travel.config.TestConfiguration;
import com.epam.budaev.travel.model.TourType;
import com.epam.budaev.travel.repository.TourTypeRepository;
import com.epam.budaev.travel.repository.impl.util.SqlRunner;
import org.hsqldb.cmdline.SqlToolError;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestConfiguration.class })
public class TourTypeRepositoryImplTest {

    @Mock
    private TourTypeRepository mockedTourTypeReposiotry;

    @Autowired
    private TourTypeRepository tourTypeRepository;


    @Before
    public void init() throws SQLException, ClassNotFoundException, IOException, SqlToolError {
        initMocks(this);
        Class.forName("org.hsqldb.jdbc.JDBCDriver");
        runSqlScript("/test-create-script.sql");
    }


    @After
    public void destroy() throws SQLException, IOException, SqlToolError {
        runSqlScript("/test-dropall-script.sql");
    }

    private void runSqlScript(String scriptPath) throws SQLException, IOException, SqlToolError {
        try (Connection connection = getConnection()) {
            new SqlRunner().runSqlScript(connection, scriptPath);
        }
    }

    private static Connection getConnection() throws SQLException {
        return DriverManager.getConnection("jdbc:hsqldb:mem:traveling-agency", "test", "test");
    }


    @Test
    public void shouldReturnTourTypeWhenGetById(){
        TourType tourType = tourTypeRepository.findById(1);
        assertThat(tourType.getName(), is("Wedding"));
    }

    @Test
    public void shouldGenerateIdWhenSaveTourType(){
        TourType tourType = new TourType();
        tourType.setName("Combo");
        tourTypeRepository.save(tourType);
        assertThat(tourType.getId(), is(notNullValue()));
        assertThat(tourType.getId(), is(3));
    }

    @Test
    public void shouldEditTourTypeWhenUpdate(){
        TourType tourType = tourTypeRepository.findById(1);
        TourType tourTypeForUpdate = new TourType();
        tourTypeForUpdate.setId(tourType.getId());
        tourTypeForUpdate.setName("Vine tour");
        tourTypeForUpdate.setVersion(tourType.getVersion());
        tourTypeRepository.update(tourTypeForUpdate);

        TourType updatedTourType = tourTypeRepository.findById(1);

        assertThat(updatedTourType.getName(), is("Vine tour"));
    }

    @Test
    public void shouldRemoveTourTypeWhenFoundById(){
        TourType tourType = new TourType();
        tourType.setId(1);

        mockedTourTypeReposiotry.remove(tourType);

        verify(mockedTourTypeReposiotry).remove(tourType);

    }

    @Test
    public void shouldQueryAllTourTypes(){
        List<TourType> tourTypeList = tourTypeRepository.findAll();

        assertThat(tourTypeList.size(), is(2));
    }




}