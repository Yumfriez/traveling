package com.epam.budaev.travel.repository.impl;

import com.epam.budaev.travel.config.TestConfiguration;
import com.epam.budaev.travel.model.Review;
import com.epam.budaev.travel.model.Tour;
import com.epam.budaev.travel.model.User;
import com.epam.budaev.travel.repository.ReviewRepository;
import com.epam.budaev.travel.repository.impl.util.SqlRunner;
import org.hsqldb.cmdline.SqlToolError;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestConfiguration.class })
public class ReviewRepositoryImplTest {

    @Mock
    private ReviewRepository mockedReviewRepository;

    @Autowired
    private ReviewRepository reviewRepository;


    @Before
    public void init() throws SQLException, ClassNotFoundException, IOException, SqlToolError {
        initMocks(this);
        Class.forName("org.hsqldb.jdbc.JDBCDriver");
        runSqlScript("/test-create-script.sql");
    }


    @After
    public void destroy() throws SQLException, IOException, SqlToolError {
        runSqlScript("/test-dropall-script.sql");
    }

    private void runSqlScript(String scriptPath) throws SQLException, IOException, SqlToolError {
        try (Connection connection = getConnection()) {
            new SqlRunner().runSqlScript(connection, scriptPath);
        }
    }

    private static Connection getConnection() throws SQLException {
        return DriverManager.getConnection("jdbc:hsqldb:mem:traveling-agency", "test", "test");
    }


    @Test
    public void shouldReturnReviewWhenGetById() {
        Review review = reviewRepository.findById(1);
        assertThat(review.getTour().getDescription(), is("Great event!"));
        assertThat(review.getTour().getPhoto().getId(), is(1));
        assertThat(review.getContent(), is("Great tour!"));
        assertThat(review.getUser().getId(), is(2));
    }

    @Test
    public void shouldGenerateIdWhenSaveReview() {
        Tour tour = new Tour();
        tour.setId(1);
        User user = new User();
        user.setId(1);
        Review review = new Review();
        review.setUser(user);
        review.setTour(tour);
        review.setContent("Yahoo!");
        reviewRepository.save(review);
        assertThat(review.getId(), is(notNullValue()));
        assertThat(review.getId(), is(2));
    }

    @Test
    public void shouldEditReviewWhenUpdate() {
        Review review = reviewRepository.findById(1);

        Tour tour = new Tour();
        tour.setId(review.getTour().getId());
        User user = new User();
        user.setId(review.getUser().getId());

        review.setTour(tour);
        review.setUser(user);
        review.setContent("WORST TOUR IN MY LIFE!!!");

        reviewRepository.update(review);

        Review updatedReview = reviewRepository.findById(1);
        assertThat(updatedReview.getContent(), is("WORST TOUR IN MY LIFE!!!"));

    }

    @Test
    public void shouldRemoveReviewWhenFoundById() {
        Review review = new Review();
        review.setId(1);

        mockedReviewRepository.remove(review);

        verify(mockedReviewRepository).remove(review);

    }

    @Test
    public void shouldQueryAllReviews() {
        List<Review> reviewList = reviewRepository.findAll();

        assertThat(reviewList.size(), is(1));
    }
}