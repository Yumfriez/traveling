package com.epam.budaev.travel.repository.impl;

import com.epam.budaev.travel.config.TestConfiguration;
import com.epam.budaev.travel.model.Country;
import com.epam.budaev.travel.model.Hotel;
import com.epam.budaev.travel.repository.HotelRepository;
import com.epam.budaev.travel.repository.impl.util.SqlRunner;
import org.hsqldb.cmdline.SqlToolError;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestConfiguration.class })
public class HotelRepositoryImplTest {

    @Mock
    private HotelRepository mockedHotelRepository;

    @Autowired
    private HotelRepository hotelRepository;


    @Before
    public void init() throws SQLException, ClassNotFoundException, IOException, SqlToolError {
        initMocks(this);
        Class.forName("org.hsqldb.jdbc.JDBCDriver");
        runSqlScript("/test-create-script.sql");
    }


    @After
    public void destroy() throws SQLException, IOException, SqlToolError {
        runSqlScript("/test-dropall-script.sql");
    }

    private void runSqlScript(String scriptPath) throws SQLException, IOException, SqlToolError {
        try (Connection connection = getConnection()) {
            new SqlRunner().runSqlScript(connection, scriptPath);
        }
    }

    private static Connection getConnection() throws SQLException {
        return DriverManager.getConnection("jdbc:hsqldb:mem:traveling-agency", "test", "test");
    }


    @Test
    public void shouldReturnHotelWhenGetById() {
        Hotel hotel = hotelRepository.findById(1);
        assertThat(hotel.getName(), is("Rainbow"));
    }

    @Test
    public void shouldGenerateIdWhenSaveHotel() {
        Hotel hotel = new Hotel();
        hotel.setName("Spring");
        hotel.setPhoneNumber("80291111111");
        hotel.setStars(3);
        Country country = new Country();
        country.setId(2);
        hotel.setCountry(country);
        hotelRepository.save(hotel);
        assertThat(hotel.getId(), is(notNullValue()));
        assertThat(hotel.getId(), is(3));
    }

    @Test
    public void shouldEditHotelWhenUpdate() {
        Hotel hotel = hotelRepository.findById(1);
        Hotel hotelForUpdate = new Hotel();
        hotelForUpdate.setId(hotel.getId());
        hotelForUpdate.setName("Spring");
        hotelForUpdate.setPhoneNumber("80291111111");
        hotelForUpdate.setStars(3);
        hotelForUpdate.setVersion(hotel.getVersion());
        Country country = new Country();
        country.setId(2);
        hotelForUpdate.setCountry(country);
        hotelRepository.update(hotelForUpdate);

        Hotel updatedHotel = hotelRepository.findById(1);

        assertThat(updatedHotel.getName(), is("Spring"));
        assertThat(updatedHotel.getPhoneNumber(), is("80291111111"));
        assertThat(updatedHotel.getStars(), is(3));
        assertThat(updatedHotel.getCountry().getId(), is(2));

    }

    @Test
    public void shouldRemoveHotelWhenFoundById() {
        Hotel hotel = new Hotel();
        hotel.setId(1);

        mockedHotelRepository.remove(hotel);

        verify(mockedHotelRepository).remove(hotel);

    }

    @Test
    public void shouldQueryAllHotels() {
        List<Hotel> hotelList = hotelRepository.findAll();

        assertThat(hotelList.size(), is(2));
    }
}