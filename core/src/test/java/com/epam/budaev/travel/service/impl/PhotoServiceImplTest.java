package com.epam.budaev.travel.service.impl;

import com.epam.budaev.travel.model.Photo;
import com.epam.budaev.travel.repository.PhotoRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Answers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

public class PhotoServiceImplTest {

    @Mock(answer = Answers.RETURNS_SMART_NULLS)
    private PhotoRepository photoRepository;

    @InjectMocks
    private PhotoServiceImpl photoService;

    @Before
    public void setUp(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void addPhoto() {

        Photo photo = new Photo();
        photo.setName("venus.jpg");
        photoService.addPhoto(photo);

        verify(photoRepository).save(photo);
    }

    @Test
    public void removePhoto() {
        Photo photo = new Photo();

        photoService.removePhoto(photo);

        verify(photoRepository).remove(photo);
    }

    @Test
    public void updatePhoto() {
        Photo photo = new Photo();
        photo.setName("pacific.jpg");

        photoService.updatePhoto(photo);

        verify(photoRepository).save(photo);
    }

    @Test
    public void findPhotoById() {

        final int id = 222;

        doReturn(new Photo()).when(photoRepository).findById(id);

        Photo photo = photoService.findPhotoById(id);

        verify(photoRepository).findById(222);
        assertThat(photo, is(notNullValue()));
    }

    @Test
    public void findAllPhotos() {
        doReturn(new ArrayList<Photo>()).when(photoRepository).findAll();

        List<Photo> photos = photoService.findAllPhotos();

        verify(photoRepository).findAll();
        assertThat(photos, is(notNullValue()));
    }

}