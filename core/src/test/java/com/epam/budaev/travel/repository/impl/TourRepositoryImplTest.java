package com.epam.budaev.travel.repository.impl;

import com.epam.budaev.travel.config.TestConfiguration;
import com.epam.budaev.travel.dto.TourDto;
import com.epam.budaev.travel.model.*;
import com.epam.budaev.travel.repository.TourRepository;
import com.epam.budaev.travel.repository.impl.util.SqlRunner;
import org.hsqldb.cmdline.SqlToolError;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestConfiguration.class })
public class TourRepositoryImplTest {

    @Mock
    private TourRepository mockedTourRepository;

    @Autowired
    private TourRepository tourRepository;


    @Before
    public void init() throws SQLException, ClassNotFoundException, IOException, SqlToolError {
        initMocks(this);
        Class.forName("org.hsqldb.jdbc.JDBCDriver");
        runSqlScript("/test-create-script.sql");
    }


    @After
    public void destroy() throws SQLException, IOException, SqlToolError {
        runSqlScript("/test-dropall-script.sql");
    }

    private void runSqlScript(String scriptPath) throws SQLException, IOException, SqlToolError {
        try (Connection connection = getConnection()) {
            new SqlRunner().runSqlScript(connection, scriptPath);
        }
    }

    private static Connection getConnection() throws SQLException {
        return DriverManager.getConnection("jdbc:hsqldb:mem:traveling-agency", "test", "test");
    }

    @Test
    public void shouldReturnListOfToursWhenFindByCriteria() {
        TourDto tourDto = new TourDto();
        tourDto.setBeginningDate(Date.valueOf(LocalDate.of(2018,5,25)));
        tourDto.setCountryName("Belarus");
        tourDto.setDuration(6);
        tourDto.setStars(4);
        tourDto.setTourTypeName("Event");

        assertThat(tourRepository.findToursByCriteria(tourDto).size(), is(1));
    }


    @Test
    public void shouldReturnTourWhenGetById() {
        Tour tour = tourRepository.findById(1);
        assertThat(tour.getDescription(), is("Great event!"));
        LocalDate localDate = tour.getBeginningDate().toLocalDate();
        assertThat(tour.getDuration(), is(6));
        assertThat(localDate.getYear(), is(2018));
        assertThat(localDate.getMonthValue(), is(5));
        assertThat(localDate.getDayOfMonth(), is(25));
        assertThat(tour.getHotel().getId(), is(1));
        assertThat(tour.getPhoto().getId(), is(1));
    }

    @Test
    public void shouldGenerateIdWhenSaveTour() {
        Hotel hotel = new Hotel();
        hotel.setId(2);
        hotel.setName("Sam Town");
        hotel.setPhoneNumber("80297777777");
        hotel.setStars(3);
        Country country = new Country();
        country.setId(2);
        country.setName("Russia");
        hotel.setCountry(country);
        Photo photo = new Photo();
        photo.setId(2);
        photo.setName("Russia.jpg");
        TourType tourType = new TourType();
        tourType.setId(1);
        Tour tour = new Tour();
        tour.setDescription("Cool tour!");
        tour.setPhoto(photo);
        tour.setHotel(hotel);
        tour.setTourType(tourType);
        tour.setBeginningDate(LocalDate.of(2018, 5 , 29));
        tour.setDuration(5);
        tour.setCost(new BigDecimal(255));
        tourRepository.save(tour);
        assertThat(tour.getId(), is(notNullValue()));
        assertThat(tour.getId(), is(2));
    }

    @Test
    public void shouldEditTourWhenUpdate() {
        Tour tour = tourRepository.findById(1);

        Hotel hotel = new Hotel();
        hotel.setId(2);
        hotel.setName("Sam Town");
        hotel.setName("Sam Town");
        hotel.setPhoneNumber("80297777777");
        hotel.setStars(3);
        Country country = new Country();
        country.setId(2);
        country.setName("Russia");
        hotel.setCountry(country);
        Photo photo = new Photo();
        photo.setId(2);
        photo.setName("Russia.jpg");
        TourType tourType = new TourType();
        tourType.setId(1);

        Tour tourForUpdate = new Tour();
        tourForUpdate.setId(tour.getId());
        tourForUpdate.setDescription("Cool tour!");
        tourForUpdate.setPhoto(photo);
        tourForUpdate.setHotel(hotel);
        tourForUpdate.setTourType(tourType);
        tourForUpdate.setBeginningDate(LocalDate.of(2018, 5 , 29));
        tourForUpdate.setDuration(5);
        tourForUpdate.setCost(new BigDecimal(255.001));
        tourForUpdate.setVersion(tour.getVersion());

        tourRepository.update(tourForUpdate);

        Tour updatedTour = tourRepository.findById(1);
        LocalDate localDate = updatedTour.getBeginningDate().toLocalDate();
        assertThat(updatedTour.getDuration(), is(5));
        assertThat(localDate.getYear(), is(2018));
        assertThat(localDate.getMonthValue(), is(5));
        assertThat(localDate.getDayOfMonth(), is(29));
        assertThat(updatedTour.getHotel().getId(), is(2));
        assertThat(updatedTour.getPhoto().getId(), is(2));
        assertThat(updatedTour.getCost().toString(), is("255.0010000000"));

    }

    @Test
    public void shouldRemoveTourWhenFoundById() {
        Tour tour = new Tour();
        tour.setId(1);

        mockedTourRepository.remove(tour);

        verify(mockedTourRepository).remove(tour);

    }

    @Test
    public void shouldQueryAllTours() {
        List<Tour> tourList = tourRepository.findAll();

        assertThat(tourList.size(), is(1));
    }
}