package com.epam.budaev.travel.repository.impl;

import com.epam.budaev.travel.config.TestConfiguration;
import com.epam.budaev.travel.model.Photo;
import com.epam.budaev.travel.repository.PhotoRepository;
import com.epam.budaev.travel.repository.impl.util.SqlRunner;
import org.hsqldb.cmdline.SqlToolError;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestConfiguration.class })
public class PhotoRepositoryImplTest {

    @Mock
    private PhotoRepository mockedPhotoRepository;

    @Autowired
    private PhotoRepository photoRepository;


    @Before
    public void init() throws SQLException, ClassNotFoundException, IOException, SqlToolError {
        initMocks(this);
        Class.forName("org.hsqldb.jdbc.JDBCDriver");
        runSqlScript("/test-create-script.sql");
    }


    @After
    public void destroy() throws SQLException, IOException, SqlToolError {
        runSqlScript("/test-dropall-script.sql");
    }

    private void runSqlScript(String scriptPath) throws SQLException, IOException, SqlToolError {
        try (Connection connection = getConnection()) {
            new SqlRunner().runSqlScript(connection, scriptPath);
        }
    }

    private static Connection getConnection() throws SQLException {
        return DriverManager.getConnection("jdbc:hsqldb:mem:traveling-agency", "test", "test");
    }


    @Test
    public void shouldReturnPhotoWhenGetById(){
        Photo country = photoRepository.findById(1);
        assertThat(country.getName(), is("Belarus.jpg"));
    }

    @Test
    public void shouldGenerateIdWhenSavePhoto(){
        Photo photo = new Photo();
        photo.setName("Norway.jpg");
        photoRepository.save(photo);
        assertThat(photo.getId(), is(notNullValue()));
        assertThat(photo.getId(), is(3));
    }

    @Test
    public void shouldEditPhotoWhenUpdate(){
        Photo photo = photoRepository.findById(1);
        Photo photoForUpdate = new Photo();
        photoForUpdate.setId(photo.getId());
        photoForUpdate.setName("Poland.jpg");
        photoForUpdate.setVersion(photo.getVersion());
        photoRepository.update(photoForUpdate);

        Photo updatedPhoto = photoRepository.findById(1);

        assertThat(updatedPhoto.getName(), is("Poland.jpg"));
    }

    @Test
    public void shouldRemovePhotoWhenFoundById(){
        Photo photo = new Photo();
        photo.setId(1);

        mockedPhotoRepository.remove(photo);

        verify(mockedPhotoRepository).remove(photo);

    }

    @Test
    public void shouldQueryAllPhotos(){
        List<Photo> photoList = photoRepository.findAll();

        assertThat(photoList.size(), is(2));
    }




}