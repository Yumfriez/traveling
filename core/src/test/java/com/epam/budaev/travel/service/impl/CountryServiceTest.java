package com.epam.budaev.travel.service.impl;

import com.epam.budaev.travel.model.Country;
import com.epam.budaev.travel.repository.CountryRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Answers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;


public class CountryServiceTest {

    private static final String COUNTRY_BELARUS = "Belarus";

    @Mock(answer = Answers.RETURNS_SMART_NULLS)
    private CountryRepository countryRepository;

    @InjectMocks
    private CountryServiceImpl countryService;

    @Before
    public void setUp(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testAddCountry()  {
        Country country = new Country();
        country.setName(COUNTRY_BELARUS);

        countryService.addCountry(country);

        verify(countryRepository).save(country);

    }

    @Test
    public void testUpdateCountry()  {

        Country country = new Country();
        country.setName(COUNTRY_BELARUS);

        countryService.updateCountry(country);
        verify(countryRepository).save(country);
    }

    @Test
    public void testFindCountryById()  {
        final int id = 222;

        doReturn(new Country()).when(countryRepository).findById(id);

        Country country = countryService.findCountryById(id);

        verify(countryRepository).findById(222);
        assertThat(country, is(notNullValue()));
    }

    @Test
    public void testRemoveCountry()  {
        Country country = new Country();
        country.setName(COUNTRY_BELARUS);

        countryService.removeCountry(country);

        verify(countryRepository).remove(country);
    }

    @Test
    public void testFindAllCountries()  {
        doReturn(new ArrayList<Country>()).when(countryRepository).findAll();

        List<Country> countries = countryService.findAllCountries();

        verify(countryRepository).findAll();
        assertThat(countries, is(notNullValue()));
    }

}