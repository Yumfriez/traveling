package com.epam.budaev.travel.controller;

import com.epam.budaev.travel.model.TourType;
import com.epam.budaev.travel.service.TourTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping(value = "/type")
public class TourTypeController {

    @Autowired
    private TourTypeService tourTypeService;

    @GetMapping
    public String showAllTourTypes(Model model) {
        List<TourType> typeList = tourTypeService.findAllTourTypes();

        model.addAttribute("types", typeList);
        return "type/main";
    }

    @GetMapping(value = "/{id}")
    public String showTourType(@PathVariable Integer id, Model model) {
        TourType tourType = tourTypeService.findTourTypeById(id);

        model.addAttribute("type", tourType);
        return "type/type";
    }
}
