package com.epam.budaev.travel.controller;

import com.epam.budaev.travel.dto.TourDto;
import com.epam.budaev.travel.exception.TravelAgencyException;
import com.epam.budaev.travel.model.Country;
import com.epam.budaev.travel.model.Tour;
import com.epam.budaev.travel.model.TourType;
import com.epam.budaev.travel.service.CountryService;
import com.epam.budaev.travel.service.HotelService;
import com.epam.budaev.travel.service.TourService;
import com.epam.budaev.travel.service.TourTypeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping(value = "/tour")
public class TourController {

    private final static Logger logger = LoggerFactory.getLogger(TourController.class);

    @Autowired
    TourService tourService;

    @Autowired
    CountryService countryService;

    @Autowired
    TourTypeService tourTypeService;

    @Autowired
    HotelService hotelService;

    @GetMapping
    public String showAllTours(Model model) {

        List<Tour> tours = tourService.findAllTours();

        model.addAttribute("tours", tours);
        return "tour/main";
    }

    @PostMapping(value = "/file/upload")
    public String tourFileUploaded(@RequestParam("file") MultipartFile file) {

        try {
            String filePath = uploadFile(file);
            tourService.loadToursFromFile(filePath);
        } catch (IOException e) {
            logger.error(e.getMessage());
        }

        return "redirect:/";
    }

    @GetMapping(value = "/search")
    public String anotherPage(Model model) {

        List<Country> countries = countryService.findAllCountries();
        List<String> countriesNames = countries.stream().map(Country::getName).collect(Collectors.toList());
        model.addAttribute("countries", countriesNames);
        List<TourType> typeList = tourTypeService.findAllTourTypes();
        List<String> typeNames = typeList.stream().map(TourType::getName).collect(Collectors.toList());
        model.addAttribute("tourTypes", typeNames);

        model.addAttribute("tour", new TourDto());

        return "tour/search";

    }

    @GetMapping(value = "/{id}")
    public String showTour(@PathVariable Integer id, Model model) {
        Tour tour = tourService.findTourById(id);

        model.addAttribute("tour", tour);
        return "tour/tour";
    }

    @GetMapping(value = "/find")
    public String findTour(@ModelAttribute TourDto tourDto, Model model) {

        List<Tour> tours = tourService.findToursByCriteria(tourDto);
        model.addAttribute("tours", tours);
        return "tour/result";

    }

    private String uploadFile(MultipartFile file) throws IOException {
        if (file == null || file.getSize() == 0) {
            throw new TravelAgencyException("File is empty");
        }
        try (InputStream inputStream = file.getInputStream()) {
            File newFile = new File("D:" + File.separator + file.getOriginalFilename());
            if (!newFile.exists()) {
                newFile.createNewFile();
            }
            try (OutputStream outputStream = new FileOutputStream(newFile)) {
                int read;
                byte[] bytes = new byte[1024];

                while ((read = inputStream.read(bytes)) != -1) {
                    outputStream.write(bytes, 0, read);
                }
            }

            return newFile.getAbsolutePath();
        }
    }


}
