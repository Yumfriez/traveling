package com.epam.budaev.travel.controller;

import com.epam.budaev.travel.model.Review;
import com.epam.budaev.travel.service.ReviewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping(value = "/review")
public class ReviewController {

    @Autowired
    private ReviewService reviewService;


    @GetMapping
    public String showAllReviews(Model model) {

        List<Review> reviewList = reviewService.findAllReviews();

        model.addAttribute("reviews", reviewList);
        return "review/main";
    }
}
