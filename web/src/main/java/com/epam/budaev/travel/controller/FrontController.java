package com.epam.budaev.travel.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "/")
public class FrontController {

    @GetMapping
    public String mainPage() {
        return "main";
    }
}
