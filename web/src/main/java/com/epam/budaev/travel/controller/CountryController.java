package com.epam.budaev.travel.controller;

import com.epam.budaev.travel.model.Country;
import com.epam.budaev.travel.service.CountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping(value = "/country")
public class CountryController {

    @Autowired
    private CountryService countryService;

    @GetMapping
    public String showAllCountries(Model model) {
        List<Country> countryList = countryService.findAllCountries();
        model.addAttribute("countries", countryList);
        return "country/main";
    }

    @GetMapping(value = "/{id}")
    public String showCountry(@PathVariable Integer id, Model model) {
        Country country = countryService.findCountryById(id);

        model.addAttribute("country", country);

        return "country/country";

    }
}
