package com.epam.budaev.travel.controller;

import com.epam.budaev.travel.model.Hotel;
import com.epam.budaev.travel.service.HotelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping(value = "/hotel")
public class HotelController {

    @Autowired
    private HotelService hotelService;

    @GetMapping
    public String showAllHotels(Model model) {

        List<Hotel> hotels = hotelService.findAllHotels();

        model.addAttribute("hotels", hotels);
        return "hotel/main";
    }

    @GetMapping(value = "/{id}")
    public String showHotel(@PathVariable Integer id, Model model) {
        Hotel hotel = hotelService.findHotelById(id);

        model.addAttribute("hotel", hotel);
        return "hotel/hotel";
    }
}
