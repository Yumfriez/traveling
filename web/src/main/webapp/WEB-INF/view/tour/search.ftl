<!DOCTYPE html>
<#import "/spring.ftl" as spring />
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Tour search</title>
    <link href="/resources/style/style.css" rel="stylesheet">
</head>
<body>

<div class="main-content">
    <form action="/tour/find" method="GET">
        <div class="section">
            <div>
                <label for="country">Страна:</label>
            <@spring.formSingleSelect "tour.countryName" countries ""/>
            </div>

            <div>
                <label for="beginningDate">Дата отправления:</label>
            <@spring.bind "tour.beginningDate"/>
                <input id="beginningDate" name="beginningDate" type="date">
                <label for="days-count">Количество суток:</label>
            <@spring.formInput "tour.duration" "" "text"/>
            </div>
        </div>
        <div class="section">
            <div>
                <label>Категория отеля:</label>
                <input name="stars" type="radio" value="1">
                <input name="stars" type="radio" value="2">
                <input name="stars" type="radio" value="3">
                <input name="stars" type="radio" value="4">
                <input name="stars" type="radio" value="5">
            </div>
            <div>

            </div>
        </div>
        <div class="section">
            <div>
            </div>
            <div class="section">
                <div>

                <@spring.formSingleSelect "tour.tourTypeName" tourTypes ""/>

                <#--<@spring.bind "tour.tourType"/>-->
                <#--<select id="types" name="types">-->
                <#--<#list tourTypes as type>-->
                <#--<option value= ${type}>${type.name}</option>-->
                <#--</#list>-->
                <#--</select>-->
                </div>

            </div>
            <button type="submit" value="Send"></button>
    </form>
</div>
</body>
</html>