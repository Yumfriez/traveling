<!DOCTYPE html>
<#import "/spring.ftl" as spring />
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Tour search</title>
    <link href="/resources/style/style.css" rel="stylesheet">
</head>
<body>

<table>
    <#list tours as tour>
    <tr>
        <td>${tour.id}</td>
        <td>${tour.beginningDate}</td>
        <td>${tour.duration}</td>
        <td>${tour.description}</td>
        <td>${tour.cost}</td>
        <td>${tour.hotel.country.name}</td>
        <td>${tour.hotel.name}</td>
        <td>${tour.tourType.name}</td>
    </tr>
    </#list>
</table>

</body>
</html>