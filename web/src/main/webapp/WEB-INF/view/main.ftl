<!DOCTYPE html>
<#import "/spring.ftl" as spring />
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Tour search</title>
    <link href="/resources/style/style.css" rel="stylesheet">
</head>
<body>
<a href="/hotel"><@spring.messageText "hotel.main" "Err" /></a>
<a href="/tour"><@spring.messageText "tour.main" "Default message" /></a>
<a href="/country"><@spring.messageText "country.main" "Default message" /></a>
<a href="/review"><@spring.messageText "review.main" "Default message" /></a>
<a href="/type"><@spring.messageText "type.main" "Default message" /></a>
<a href="/tour/search"><@spring.messageText "tour.search" "Default message" /></a>
</body>
</html>