<!DOCTYPE html>
<#import "/spring.ftl" as spring />
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Tour search</title>
    <link href="/resources/style/style.css" rel="stylesheet">
</head>
<body>
<table>
<#list reviews as review>
    <tr>
        <td>${review.id}</td>
        <td>${review.content}</td>
        <td>${review.user.login}</td>
    </tr>
</#list>
</table>
</body>
</html>