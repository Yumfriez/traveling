<!DOCTYPE html>
<#import "/spring.ftl" as spring />
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Tour search</title>
    <link href="/resources/style/style.css" rel="stylesheet">
</head>
<body>
<table>
    <tr>
        <#list countries as country>
            <td>${country.id}</td>
            <td>${country.name}</td>
        </#list>
    </tr>
</table>
</body>
</html>